import React, { createContext, useState } from "react";

export const DisplayTypeContext = createContext();

export const DisplayTypeProvider = ({ children }) => {
    const [ displayType, setDisplayType ] = useState("картки");

    return (
        <DisplayTypeContext.Provider value={{ displayType, setDisplayType}}>
            {children}
        </DisplayTypeContext.Provider>
    );
};