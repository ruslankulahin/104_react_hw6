import configureStore from "redux-mock-store";
import {
    actionAddFavorites,
    actionRemoveFromFavorites,
    actionsShowFavoritesModal,
} from "./favorites.slice.js";

/* `const mockStore = configureStore();` создает макет хранилища с помощью функции `configureStore` из
библиотеки `redux-mock-store`. Это позволяет нам создать магазин с тем же API, что и магазин Redux,
но без фактического использования Redux. Затем мы можем использовать это макетное хранилище для
отправки действий и проверки ожидаемых отправленных действий. */
const mockStore = configureStore();
const initialState = {
    favoriteItems: JSON.parse(localStorage.getItem("favoriteItems")) || [],
    showFavoritesModal: false,
};

describe("Favorites Reducers", () => {
    let store;

    /* `beforeEach(() => { store =mockStore(initialState); });` — это функция настройки, которая
    выполняется перед каждым тестовым примером в наборе тестов. Он создает макет хранилища, используя
    функцию mockStore из библиотеки redux-mock-store, и присваивает его переменной store. Макет
    хранилища инициализируется с помощью объекта «initialState», который представляет начальное
    состояние хранилища Redux. Это позволяет каждому тестовому примеру иметь чистое и согласованное
    хранилище для работы. */
    beforeEach(() => {
        store = mockStore(initialState);
    });

    it("should handle actionAddFavorites", () => {
        const itemToAdd = {
            id: 1,
            name: "Product 1",
            price: 10,
            image: "product1.jpg",
        };
        store.dispatch(actionAddFavorites(itemToAdd));
        const expectedActions = [
            {
                type: "favorites/actionAddFavorites",
                payload: itemToAdd,
            },
        ];
        expect(store.getActions()).toEqual(expectedActions);
    });

    it("should handle actionRemoveFromFavorites", () => {
        const itemToRemove = {
            id: 1,
        };
        store.dispatch(actionRemoveFromFavorites(itemToRemove));
        const expectedActions = [
            {
                type: "favorites/actionRemoveFromFavorites",
                payload: itemToRemove,
            },
        ];
        expect(store.getActions()).toEqual(expectedActions);
    });

    it("should handle actionsShowFavoritesModal", () => {
        store.dispatch(actionsShowFavoritesModal());
        const expectedActions = [
            {
                type: "favorites/actionsShowFavoritesModal",
            },
        ];
        expect(store.getActions()).toEqual(expectedActions);
    });
});
