import configureStore from "redux-mock-store";
import {
    actionAddToCart,
    actionRemoveFromCart,
    actionsRemoveItemFromCart,
    actionsClearCart,
    actionsShowCartModal,
} from "./cart.slice.js";

/* `const mockStore = configureStore();` создает макет хранилища с помощью функции `configureStore` из
библиотеки `redux-mock-store`. Это позволяет нам создать магазин с тем же API, что и магазин Redux,
но без фактического использования Redux. Затем мы можем использовать это макетное хранилище для
отправки действий и проверки ожидаемых отправленных действий. */
const mockStore = configureStore();
const initialState = {
    cartItems: JSON.parse(localStorage.getItem("cartItems")) || [],
    showCartModal: false,
};

describe("Cart Reducers", () => {
    let store;

    /* `beforeEach(() => { store =mockStore(initialState); });` — это функция настройки, которая
    выполняется перед каждым тестовым примером в наборе тестов. Он создает макет хранилища, используя
    функцию mockStore из библиотеки redux-mock-store, и присваивает его переменной store. Макет
    хранилища инициализируется с помощью объекта «initialState», который представляет начальное
    состояние хранилища Redux. Это позволяет каждому тестовому примеру иметь чистое и согласованное
    хранилище для работы. */
    beforeEach(() => {
        store = mockStore(initialState);
    });

    it("should handle actionAddToCart", () => {
        const itemToAdd = {
            id: 1,
            name: "Product 1",
            price: 10,
            image: "product1.jpg",
        };
        store.dispatch(actionAddToCart(itemToAdd));
        const expectedActions = [
            {
                type: "cart/actionAddToCart",
                payload: itemToAdd,
            },
        ];
        expect(store.getActions()).toEqual(expectedActions);
    });

    it("should handle actionRemoveFromCart", () => {
        const itemToRemove = {
            id: 1,
        };
        store.dispatch(actionRemoveFromCart(itemToRemove));
        const expectedActions = [
            {
                type: "cart/actionRemoveFromCart",
                payload: itemToRemove,
            },
        ];
        expect(store.getActions()).toEqual(expectedActions);
    });

    it("should handle actionsRemoveItemFromCart", () => {
        const itemToRemove = {
            id: 1,
        };
        store.dispatch(actionsRemoveItemFromCart(itemToRemove));
        const expectedActions = [
            {
                type: "cart/actionsRemoveItemFromCart",
                payload: itemToRemove,
            },
        ];
        expect(store.getActions()).toEqual(expectedActions);
    });

    it("should handle actionsClearCart", () => {
        store.dispatch(actionsClearCart());
        const expectedActions = [
            {
                type: "cart/actionsClearCart",
            },
        ];
        expect(store.getActions()).toEqual(expectedActions);
    });

    it("should create actionsShowCartModal", () => {
        const expectedAction = {
            type: "cart/actionsShowCartModal",
        };
        expect(actionsShowCartModal()).toEqual(expectedAction);
    });
});
