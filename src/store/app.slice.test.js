import rootReducers, {
    actionAddToProducts,
    actionsShowConfirmationModal,
    actionLoading,
} from "./app.slice.js";

describe("app.slice", () => {
    it("should return the initial state", () => {
        expect(rootReducers(undefined, { type: undefined })).toEqual({
            products: [],
            showConfirmationModal: false,
            isLoading: true,
        });
    });

    it("should handle a actionAddToProducts", () => {
        const previousState = {};
        expect(
            rootReducers(
                previousState,
                actionAddToProducts([{ test: 1 }, { test: 2 }])
            )
        ).toEqual({ products: [{ test: 1 }, { test: 2 }] });
    });

    it("should handle a actionsShowConfirmationModal", () => {
        const previousState = {};
        expect(
            rootReducers(previousState, actionsShowConfirmationModal(false))
        ).toEqual({ showConfirmationModal: true });
    });

    it("should handle a actionLoading", () => {
        const previousState = {};
        expect(rootReducers(previousState, actionLoading(false))).toEqual({
            isLoading: false,
        });
    });

    it("should create actionAddToProducts with payload", () => {
        const products = [{ id: 1, name: "Product 1" }];
        const expectedAction = {
            type: "app/actionAddToProducts",
            payload: products,
        };
        expect(actionAddToProducts(products)).toEqual(expectedAction);
    });

    it("should create actionsShowConfirmationModal", () => {
        const expectedAction = {
            type: "app/actionsShowConfirmationModal",
        };
        expect(actionsShowConfirmationModal()).toEqual(expectedAction);
    });

    it("should create actionLoading with payload", () => {
        const isLoading = true;
        const expectedAction = {
            type: "app/actionLoading",
            payload: isLoading,
        };
        expect(actionLoading(isLoading)).toEqual(expectedAction);
    });
});
