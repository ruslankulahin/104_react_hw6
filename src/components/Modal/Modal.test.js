import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import Modal from "./Modal";

describe("Modal component", () => {
    // Тестування базової рендерингу Modal без параметрів:
    it("Modal renders without crashing", () => {
        render(<Modal />);
    });

    // Тестування модального вікна "Кошик"

    it("Modal renders with header", () => {
        render(<Modal header="Кошик" />);

        expect(screen.getByText("Кошик")).toBeInTheDocument();
    });

    it("Modal close button works", () => {
        const closeModalMock = jest.fn();
        render(
            <Modal
                header="Кошик"
                closeButton={true}
                closeModal={closeModalMock}
            />
        );
        const closeButton = screen.getByTestId("modal-close");
        fireEvent.click(closeButton);
        expect(closeModalMock).toHaveBeenCalledTimes(1);
    });

    it("Modal handles click outside", () => {
        const handleOutsideClickMock = jest.fn();
        render(
            <Modal header="Кошик" handleOutsideClick={handleOutsideClickMock} />
        );

        const modalWrapper = screen.getByTestId("modal");
        fireEvent.click(modalWrapper);
        expect(handleOutsideClickMock).toHaveBeenCalledTimes(1);
    });

    it("renders with custom classNames", () => {
        const { getByTestId } = render(<Modal classNames="modal-wrapper" />);
        expect(getByTestId("modal")).toHaveClass("modal-wrapper");
    });

    it("renders with close button", () => {
        const { getByTestId } = render(
            <Modal closeButton={<button>Close</button>} />
        );
        expect(getByTestId("modal-close")).toBeInTheDocument();
    });

    it("renders with custom totalPrice", () => {
        const { getByText } = render(<Modal totalPrice={<div>$100</div>} />);
        expect(getByText("$100")).toBeInTheDocument();
    });

    it("renders with custom text", () => {
        const { getByText } = render(<Modal text={<div>Custom Text</div>} />);
        expect(getByText("Custom Text")).toBeInTheDocument();
    });

    it("renders with custom actions", () => {
        const { getByText } = render(
            <Modal actions={<button>Custom Action</button>} />
        );
        expect(getByText("Custom Action")).toBeInTheDocument();
    });
});
