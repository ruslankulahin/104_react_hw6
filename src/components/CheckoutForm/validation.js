import * as yup from "yup";

export const validationSchema = yup.object({
    firstName: yup.string()
      .min(2, "Коротке ім`я!")
      .max(15, "Задовге ім`я!")
      .required("Поле є обов`язковим для заповнення"),
    lastName: yup.string()
      .min(2, "Коротке!")
      .max(25, "Задовге прізвище!")
      .required("Поле є обов`язковим для заповнення"),
    age: yup.number()
      .required("Поле є обов`язковим для заповнення")
      .positive("Вік повинен бути додатнім числом")
      .integer("Вік повинен бути цілим числом"),
    address: yup.string().required("Поле є обов`язковим для заповнення"),
    phone: yup.string()
      .required("Поле є обов`язковим для заповнення")
      .matches(/^\d{10}$/, "Неправильний номер телефону (###)###-##-##"),
});