import React, { useContext } from "react";
import { DisplayTypeContext } from "../../context/context.jsx";
import PropTypes from "prop-types";
import Button from "../Button/Button.jsx";
import ProductCard from "../ProductCard";
import "./ProductList.scss";

export default function ProductList({products}) {

    const { displayType, setDisplayType } = useContext(DisplayTypeContext);

    const toggleDisplayType = () => {
        setDisplayType(prevDisplayType => prevDisplayType === "картки" ? "таблиця": "картки" );
    };

    return (       
        <div className="product-list-wrapper">
            <Button classNames="display-type-btn" onClick={toggleDisplayType}>
                {displayType === "картки" ? "Таблиця" : "Картки"}
            </Button>

            {displayType === "картки" ? (
                <div className="container">
                    <div className="product-list">
                        {products.map((product) => (
                            <ProductCard
                                classNames="product-card" 
                                key={product.id}
                                product={product}
                                image = {
                                    <div className="image-wrapper">
                                        <img src={product.image} alt={product.name} />
                                    </div>
                                }
                                productName={
                                    <h3 className="product-name">{product.name}</h3>
                                }
                            />
                        ))}
                    </div>
                </div>
            ) : (
                <div className="container">
                    <div className="product-list-table">
                         {products.map((product) => (
                            <ProductCard
                                classNames="product-table__item" 
                                key={product.id}
                                product={product}
                                image={
                                    <div className="product-table__image">
                                        <img className="img" src={product.image} alt={product.name} />
                                    </div>
                                }
                                productName={
                                    <h3 className="product-table__name ">{product.name}</h3>
                                }
                            />
                        ))}
                    </div>
                </div>
            )
            }
        </div>
    );
};

ProductList.propTypes = {
    products: PropTypes.array,
};
