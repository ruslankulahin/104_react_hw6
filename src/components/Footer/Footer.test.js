import React from 'react';
import renderer from 'react-test-renderer';
import Footer from './Footer';

describe("Footer component snapshot", () => {
	it("renders correctly", () => {
	   const tree = renderer.create(<Footer />).toJSON();
	   expect(tree).toMatchSnapshot();
	});
});



// // Використання react-test-renderer/shallow:

// import React from "react";
// import ShallowRenderer from "react-test-renderer/shallow";
// import Footer from "./Footer";

// test("Footer component snapshot", () => {
//   const renderer = new ShallowRenderer();
//   renderer.render(<Footer />);
//   const result = renderer.getRenderOutput();
//   expect(result).toMatchSnapshot();
// });






