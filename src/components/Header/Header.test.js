import React from 'react';
import {render} from "@testing-library/react";
import {Provider} from 'react-redux'
import {BrowserRouter} from "react-router-dom";
import store from "../../store/index.js";
import Header from "./Header"

describe("Testing Header", ()=> {
	it("Snapshots", ()=> {
		const header = render(<BrowserRouter><Provider store={store}><Header/></Provider></BrowserRouter>)
		expect(header).toMatchSnapshot()
	})
})
