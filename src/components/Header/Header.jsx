import React from "react";
import PropTypes from "prop-types";
import {
    faCartShopping,
    faStar,
    faArrowLeft,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import {
    selectFavoritesItems,
    selectCartItems,
} from "../../selectors/index.js";
import { actionsShowFavoritesModal } from "../../store/favorites.slice.js";
import { actionsShowCartModal } from "../../store/cart.slice.js";
import "./Header.scss";

export default function Header({ cartItemTotal }) {
    const dispatch = useDispatch();
    const favoriteItems = useSelector(selectFavoritesItems);
    const cartItems = useSelector(selectCartItems);

    const navigate = useNavigate();
    const goBack = () => navigate(-1);

    return (
        <header className="header">
            <div className="container header-wrapper">
                <h1 className="title">
                    <span> THE BEST</span>
                    <br></br> products <span>ONLY FOR YOU</span>
                </h1>
                <nav className="header-navigation">
                    <span onClick={goBack} className="btn-back">
                        <FontAwesomeIcon
                            icon={faArrowLeft}
                            size="2xs"
                            style={{ color: "#ccc" }}
                        />
                    </span>
                    <div className="pages-navigation">
                        <Link to="/" className="nav-link df-fdc-jcc-aie">
                            Головна
                        </Link>
                        <Link to="/cart" className="nav-link df-fdc-jcc-aie">
                            <FontAwesomeIcon
                                icon={faCartShopping}
                                size="sm"
                                style={{
                                    marginRight: "5px",
                                    color: "#ccc",
                                }}
                            />
                            Кошик
                        </Link>
                        <Link
                            to="/favorites"
                            className="nav-link df-fdc-jcc-aie"
                        >
                            <FontAwesomeIcon
                                icon={faStar}
                                size="sm"
                                style={{
                                    marginRight: "5px",
                                    color: "#ccc",
                                }}
                            />
                            Обрані
                        </Link>
                    </div>
                </nav>
                <div className="icons-wrapper">
                    <div
                        className="cart-shopping-wrapper"
                        onClick={() => dispatch(actionsShowCartModal())}
                    >
                        <FontAwesomeIcon
                            icon={faCartShopping}
                            size="sm"
                            style={{
                                color: cartItems.length ? "#f4f88b" : "#ddd",
                            }}
                        />{" "}
                        <span
                            style={{
                                color: cartItems.length ? "blue" : "#ddd",
                            }}
                        >
                            {cartItemTotal}
                        </span>
                    </div>
                    <div
                        className="star-wrapper"
                        onClick={() => dispatch(actionsShowFavoritesModal())}
                    >
                        <FontAwesomeIcon
                            icon={faStar}
                            size="sm"
                            style={{
                                outlineColor: "#ddd",
                                color: favoriteItems.length
                                    ? "#f4f88b"
                                    : "#ddd",
                            }}
                        />{" "}
                        <span
                            style={{
                                color: favoriteItems.length ? "blue" : "#ddd",
                            }}
                        >
                            {favoriteItems.length}
                        </span>
                    </div>
                </div>
            </div>
        </header>
    );
}

Header.propTypes = {
    cartItemTotal: PropTypes.number,
};
