import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import Button from "./Button";

/* `const handleClick = jest.fn();` создает новую фиктивную функцию с использованием метода
`jest.fn()`. Эту фиктивную функцию можно использовать для имитации события щелчка или вызова любой
другой функции в тесте. В этом случае он используется для имитации события щелчка на компоненте
Button и отслеживания того, была ли вызвана функция или нет, с помощью
`expect(handleClick).toHaveBeenCalled()`. */
const handleClick = jest.fn();

describe("testing Button", () => {
    test("Tag name", () => {
        const { container } = render(<Button>Додати до кошика</Button>);
        const button = container.querySelector("button");
        expect(button).toBeInTheDocument();
    });
    test("in the document", () => {
        render(<Button>Додати до кошика</Button>);
        expect(screen.getByText("Додати до кошика")).toBeInTheDocument();
    });
    test("testing add class", () => {
        render(<Button classNames>Додати до кошика</Button>);
        expect(screen.getByText("Додати до кошика")).toHaveClass("btn");
    });
    test("testing type button", () => {
        render(<Button>Додати до кошика</Button>);
        expect(screen.getByText("Додати до кошика")).toHaveAttribute(
            "type",
            "button"
        );
    });

    test("Button click", () => {
        const { container } = render(
            <Button onClick={handleClick}>Додати до кошика</Button>
        );
        const button = container.firstChild;
        fireEvent.click(button);
        expect(handleClick).toHaveBeenCalled();
    });

    test("Tag name", () => {
        const { container } = render(<Button>Товар у кошику</Button>);
        const button = container.querySelector("button");
        expect(button).toBeInTheDocument();
    });
    test("in the document", () => {
        render(<Button>Товар у кошику</Button>);
        expect(screen.getByText("Товар у кошику")).toBeInTheDocument();
    });
    test("testing add class", () => {
        render(<Button classNames>Товар у кошику</Button>);
        expect(screen.getByText("Товар у кошику")).toHaveClass("btn");
    });
    test("testing type button", () => {
        render(<Button>Товар у кошику</Button>);
        expect(screen.getByText("Товар у кошику")).toHaveAttribute(
            "type",
            "button"
        );
    });

    test("Button click", () => {
        const { container } = render(
            <Button onClick={handleClick}>Товар у кошику</Button>
        );
        const button = container.firstChild;
        fireEvent.click(button);
        expect(handleClick).toHaveBeenCalled();
    });

    test("Tag name", () => {
        const { container } = render(<Button>-</Button>);
        const button = container.querySelector("button");
        expect(button).toBeInTheDocument();
    });
    test("in the document", () => {
        render(<Button>-</Button>);
        expect(screen.getByText("-")).toBeInTheDocument();
    });
    test("testing add class", () => {
        render(<Button classNames>-</Button>);
        expect(screen.getByText("-")).toHaveClass("btn");
    });
    test("testing type button", () => {
        render(<Button>-</Button>);
        expect(screen.getByText("-")).toHaveAttribute("type", "button");
    });

    test("Button click", () => {
        const { container } = render(<Button onClick={handleClick}>-</Button>);
        const button = container.firstChild;
        fireEvent.click(button);
        expect(handleClick).toHaveBeenCalled();
    });

    test("Tag name", () => {
        const { container } = render(<Button>+</Button>);
        const button = container.querySelector("button");
        expect(button).toBeInTheDocument();
    });
    test("in the document", () => {
        render(<Button>+</Button>);
        expect(screen.getByText("+")).toBeInTheDocument();
    });
    test("testing add class", () => {
        render(<Button classNames>+</Button>);
        expect(screen.getByText("+")).toHaveClass("btn");
    });
    test("testing type button", () => {
        render(<Button>+</Button>);
        expect(screen.getByText("+")).toHaveAttribute("type", "button");
    });

    test("Button click", () => {
        const { container } = render(<Button onClick={handleClick}>+</Button>);
        const button = container.firstChild;
        fireEvent.click(button);
        expect(handleClick).toHaveBeenCalled();
    });

    test("Tag name", () => {
        const { container } = render(<Button>Додати ще?</Button>);
        const button = container.querySelector("button");
        expect(button).toBeInTheDocument();
    });
    test("in the document", () => {
        render(<Button>Додати ще?</Button>);
        expect(screen.getByText("Додати ще?")).toBeInTheDocument();
    });
    test("testing add class", () => {
        render(<Button classNames>Додати ще?</Button>);
        expect(screen.getByText("Додати ще?")).toHaveClass("btn");
    });
    test("testing type button", () => {
        render(<Button>Додати ще?</Button>);
        expect(screen.getByText("Додати ще?")).toHaveAttribute(
            "type",
            "button"
        );
    });

    test("Button click", () => {
        const { container } = render(
            <Button onClick={handleClick}>Додати ще?</Button>
        );
        const button = container.firstChild;
        fireEvent.click(button);
        expect(handleClick).toHaveBeenCalled();
    });

    test("Tag name", () => {
        const { container } = render(<Button>Додати до кошика</Button>);
        const button = container.querySelector("button");
        expect(button).toBeInTheDocument();
    });
    test("in the document", () => {
        render(<Button>Додати до кошика</Button>);
        expect(screen.getByText("Додати до кошика")).toBeInTheDocument();
    });
    test("testing add class", () => {
        render(<Button classNames>Додати до кошика</Button>);
        expect(screen.getByText("Додати до кошика")).toHaveClass("btn");
    });
    test("testing type button", () => {
        render(<Button>Додати до кошика</Button>);
        expect(screen.getByText("Додати до кошика")).toHaveAttribute(
            "type",
            "button"
        );
    });

    test("Button click", () => {
        const { container } = render(
            <Button onClick={handleClick}>Додати до кошика</Button>
        );
        const button = container.firstChild;
        fireEvent.click(button);
        expect(handleClick).toHaveBeenCalled();
    });
});
