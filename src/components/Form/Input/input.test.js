import React from "react";
import { render, screen } from "@testing-library/react";
import InputBox from "./Input";

describe("Testing input component", () => {
    test("In the document", () => {
        render(<InputBox label={"Ім'я"} name={"firstName"} />);
        expect(screen.getByText("Ім'я")).toBeInTheDocument();
    });
    test("Input atributes", () => {
        const { container } = render(
            <InputBox name={"firstName"} label={"Ім'я"} />
        );
        const input = container.querySelector("[name='firstName']");
        expect(input).toBeInTheDocument();
        expect(input).toHaveAttribute("type", "text");
        expect(input).toHaveClass("form-control");
    });
    test("Input atributes", () => {
        const { container } = render(
            <InputBox name={"lastName"} label={"Прізвище"} />
        );
        const input = container.querySelector("[name='lastName']");
        expect(input).toBeInTheDocument();
        expect(input).toHaveAttribute("type", "text");
        expect(input).toHaveClass("form-control");
    });
    test("Input atributes", () => {
        const { container } = render(<InputBox name={"age"} label={"Вік"} />);
        const input = container.querySelector("[name='age']");
        expect(input).toBeInTheDocument();
        expect(input).toHaveAttribute("type", "text");
        expect(input).toHaveClass("form-control");
    });
    test("Input atributes", () => {
        const { container } = render(
            <InputBox name={"address"} label={"Адреса доставки"} />
        );
        const input = container.querySelector("[name='address']");
        expect(input).toBeInTheDocument();
        expect(input).toHaveAttribute("type", "text");
        expect(input).toHaveClass("form-control");
    });
    test("In the document", () => {
        render(<InputBox label={"Мобільний телефон"} name={"phone"} />);
        expect(screen.getByText("Мобільний телефон")).toBeInTheDocument();
    });
});
