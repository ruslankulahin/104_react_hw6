import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";
import "./Input.scss";

const InputBox = (props) => {
    const {
        classNames,
        label,
        type,
        name,
        placeholder,
        errorMessage,
        error,
        ...restProps
    } = props;

    return (
        <label
            className={cn("form-item", classNames, { "has-validation": error })}
        >
            <p className="form-label">{label}</p>
            <input
                type={type}
                className="form-control"
                name={name}
                placeholder={placeholder}
                {...restProps}
            />
            {error && <p className="error-message">{errorMessage}</p>}
        </label>
    );
};

InputBox.propTypes = {
    classNames: PropTypes.string,
    label: PropTypes.string.isRequired,
    type: PropTypes.string,
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    errorMessage: PropTypes.string,
    error: PropTypes.bool,
};

InputBox.defaultProps = {
    type: "text",
};

export default InputBox;
